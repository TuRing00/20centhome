TwentyCentApp.factory("TwentyCentFactory", function () {
    var urls = function () {
        return [
            { label: 'Google', url: 'http://www.google.com', lastVisitedDate: new Date() },
            { label: 'News', url: 'http://news.google.com', lastVisitedDate: new Date() },
            { label: 'Bing', url: 'http://www.bing.com', lastVisitedDate: new Date() },
            { label: 'Yahoo', url: 'http://www.yahoo.com', lastVisitedDate: new Date() }
        ];
    };

    return {
        urls: urls
    }
});

TwentyCentApp.controller('TwentyCentController', ['$scope', function ($scope, TwentyCentFactory) {

    $scope.newUrl = null;
    $scope.resetData = function () {
        $scope.urls = TwentyCentFactory.urls();
        $scope.saveData();
    };

    $scope.clearData = function () {
        localStorage.removeItem('urls');
        $scope.urls = null;
    };

    $scope.loadData = function () {
        if (localStorage.urls) {
            $scope.urls = JSON.parse(localStorage.urls);
        } else {
            $scope.urls = null;
        }
    };

    $scope.saveData = function () {
        localStorage.urls = JSON.stringify($scope.urls);
    };

    $scope.addEntry = function () {
        $scope.newUrl.lastVisitedDate = new Date();
        $scope.urls.push($scope.newUrl);
        $scope.newUrl = null;
        $scope.saveData();
        alert("The entry is correctly added");
    };

    $scope.deleteEntry = function (url) {
        var i = $scope.urls.indexOf(url);
        $scope.urls.splice(i, 1);
        alert("The entry is correctly deleted");
    };

    $scope.editEntry = function (url) {
        var i = $scope.urls.indexOf(url);
        $scope.newUrl = angular.copy($scope.urls[i]);
    };

    $scope.tagAsVisitedToday = function (url) {
        url.lastVisitedDate = new Date();
        $scope.saveData();
    };

    $scope.notRecentlyVisited = function (url) {
        if ($scope.showAll) {
            return true;
        }
        var lastday = new Date();
        lastday.setDate(lastday.getDate() - 1);
        return url.lastVisitedDate < lastday;
    };

    $scope.loadData();
}]);

